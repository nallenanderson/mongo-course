const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017/videos';

exports.index = (req, res) => {
  res.render('index', { title: 'Create a Movie' })
}

exports.getMovies = (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if(err) return console.log('Could not connect to the server...');

    db.collection('movies').find().toArray((err, docs) => {
      if(err) return res.status(500).json({ message: 'There was an error!' });

      res.render('movies', { title: 'Movie List', movies: docs });
    })
    db.close();
  });
}

exports.createMovie = (req, res) => {
  const { title, year, imdb } = req.body;

  MongoClient.connect(url, function(err, db) {
    if(err) return console.log('Could not connect to the server...');

    db.collection('movies').insertOne({ title, year, imdb }, (err, result) => {
      if(err) return res.status(500).json({ message: 'There was an error!' });

      res.redirect('/movies');
    });
    db.close();
  });
}
