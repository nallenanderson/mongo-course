const express = require('express');
const router = express.Router();
const moviesController = require('./controllers/moviesController')

router.get('/create', moviesController.index);

router.get('/', moviesController.getMovies);
router.get('/movies', moviesController.getMovies);

router.post('/movie', moviesController.createMovie);

module.exports = router;
